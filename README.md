<!--
The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml
    roles/alpine_linux/tasks/main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
-->

Overview
--------

We provision machines largely immutably.  We install each operating system to
a disk-image file, and add each file to a repository of disk-image files.  In
order to provision a machine, we first configure our PXE services to instruct
the machine to download a customised Linux operating system, called an image
installer, and start the image installer with kernel parameters that specify
the disk image to install and the drive to install it to.  We then boot the
machine via the appropriate PXE-enabled NIC.  As the image installer runs, it
reads its kernel parameters, downloads the specified file from the repository
of disk-image files, partially nullifies each partition and drive the machine
currently has, writes the raw expansion of the downloaded disk image to the
specified drive, and reboots the machine.

In order to carry out machine configuration that depends upon the platform on
which a machine runs -- and so cannot be known when a disk image is created
-- a disk image may contain a virgin-configuration service.  The service runs
during the machine's first boot.  If it can identify the machine's platform,
it gives the machine a configuration appropriate for that platform, otherwise
it gives the machine a default configuration.  The service then deactivates
and deletes itself -- subsequent boots will not reconfigure the machine.

The machine is now almost fully and immutably provisioned.  However, not all
aspects of provisioning are currently immutable.  For the moment, we want to
be able to reconfigure the VLAN and bridge network interfaces of a machine
without necessarily reprovisioning it.  This Ansible playbook tries to ensure
that each provisioned machine has a complete network-interface configuration.
It does so by calling an Ansible role appropriate for the machine's operating
system.

The Alpine Linux role
---------------------

This Ansible role tries to ensure that each provisioned Alpine Linux machine
has a complete network-interface configuration.

Warning: Alpine Linux uses the commands `ifup` and `ifdown` to bring network
interfaces up and down.  If these commands and the command `ip` disagree on
the state of an interface -- as can happen if `ifup` brings the interface up,
the configuration of the interface changes, and `ifdown` brings the interface
down -- then the interface can become jammed in an intermediate state: `ifup`
cannot bring the interface up because `ip` considers the interface already
configured, while `ifdown` cannot bring the interface down because `ifdown`
considers the interface already unconfigured.  Unjamming a jammed interface
needs careful use of low-level interface commands at best, a machine reboot
at worst.  For this reason, edit this role carefully, and *always* test on a
non-essential machine first.

### Alpine Linux commands

Try to ensure that the machine is equipped with the current versions of the
helper commands, written specifically for Alpine Linux, that help ensure the
state of the machine.

### Platform

Try to identify the machine's platform.

### VLAN interfaces

Try to ensure that for each VLAN in our cloud environment, a VLAN interface
on the appropriate physical interface of the machine's platform exists.

### Bridge interfaces

Try to ensure that for each logical Ethernet segment in our cloud
environment, a bridge interface on the appropriate VLAN interface exists and
is up.

docs.sh
-------

The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml
    roles/alpine_linux/tasks/main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
