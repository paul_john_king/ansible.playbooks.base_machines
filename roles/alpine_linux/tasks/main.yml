# vim:ff=unix ts=2 sw=2 ai expandtab
---
#### DOC BLOCK BEGIN

# The Alpine Linux role
# ---------------------
#

# This Ansible role tries to ensure that each provisioned Alpine Linux machine
# has a complete network-interface configuration.
# 
# Warning: Alpine Linux uses the commands `ifup` and `ifdown` to bring network
# interfaces up and down.  If these commands and the command `ip` disagree on
# the state of an interface -- as can happen if `ifup` brings the interface up,
# the configuration of the interface changes, and `ifdown` brings the interface
# down -- then the interface can become jammed in an intermediate state: `ifup`
# cannot bring the interface up because `ip` considers the interface already
# configured, while `ifdown` cannot bring the interface down because `ifdown`
# considers the interface already unconfigured.  Unjamming a jammed interface
# needs careful use of low-level interface commands at best, a machine reboot
# at worst.  For this reason, edit this role carefully, and *always* test on a
# non-essential machine first.
#

# ### Alpine Linux commands
#

# Try to ensure that the machine is equipped with the current versions of the
# helper commands, written specifically for Alpine Linux, that help ensure the
# state of the machine.
#

#### DOC BLOCK END

- import_tasks: "{{busybox_tools}}/copy_plain_file.yml"
  vars:
    source: "{{commands_source_dir}}/ensure_interface_is_up"
    target: "{{commands_target_dir}}/ensure_interface_is_up"
    mode: "0755"
    owner: "root"
    group: "root"

#### DOC BLOCK BEGIN

# ### Platform
#

# Try to identify the machine's platform.
#

#### DOC BLOCK END

- import_tasks: "{{busybox_tools}}/run_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/get_platform"
    variable: "platform"

#### DOC BLOCK BEGIN

# ### VLAN interfaces
#

# Try to ensure that for each VLAN in our cloud environment, a VLAN interface
# on the appropriate physical interface of the machine's platform exists.
#

#### DOC BLOCK END

- import_tasks: "{{busybox_tools}}/write_plain_file.yml"
  when:
  - "platform is defined and (platform == 'atuin' or platform == 'minion')"
  vars:
    content: |
      #	This file contains a VLAN-interface configuration for a machine on the
      #	platform 'atuin' or 'minion'.
      
      iface vlan_902 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_902 type vlan id 902
        up ip link set dev vlan_902 up
      
        down ip link set dev vlan_902 down
        down ip link del dev vlan_902
      
      iface vlan_921 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_921 type vlan id 921
        up ip link set dev vlan_921 up
      
        down ip link set dev vlan_921 down
        down ip link del dev vlan_921
      
      iface vlan_922 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_922 type vlan id 922
        up ip link set dev vlan_922 up
      
        down ip link set dev vlan_922 down
        down ip link del dev vlan_922
      
      iface vlan_923 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_923 type vlan id 923
        up ip link set dev vlan_923 up
      
        down ip link set dev vlan_923 down
        down ip link del dev vlan_923
      
      iface vlan_927 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_927 type vlan id 927
        up ip link set dev vlan_927 up
      
        down ip link set dev vlan_927 down
        down ip link del dev vlan_927
      
      iface vlan_928 inet manual
      
        up ifup eth1
        up ip link add link eth1 name vlan_928 type vlan id 928
        up ip link set dev vlan_928 up
      
        down ip link set dev vlan_928 down
        down ip link del dev vlan_928
    target: "/etc/network/interfaces.vlan"
    mode: "0644"
    owner: "root"
    group: "root"

- import_tasks: "{{busybox_tools}}/write_plain_file.yml"
  when:
  - "platform is defined and (platform == 'cohen' or platform == 'tubul' or platform == 'vimes')"
  vars:
    content: |
      #	This file contains a VLAN-interface configuration for a machine on the
      #	platform 'cohen', 'tubul' or 'vimes'.
      
      iface vlan_902 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_902 type vlan id 902
        up ip link set dev vlan_902 up
      
        down ip link set dev vlan_902 down
        down ip link del dev vlan_902
      
      iface vlan_921 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_921 type vlan id 921
        up ip link set dev vlan_921 up
      
        down ip link set dev vlan_921 down
        down ip link del dev vlan_921
      
      iface vlan_922 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_922 type vlan id 922
        up ip link set dev vlan_922 up
      
        down ip link set dev vlan_922 down
        down ip link del dev vlan_922
      
      iface vlan_923 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_923 type vlan id 923
        up ip link set dev vlan_923 up
      
        down ip link set dev vlan_923 down
        down ip link del dev vlan_923
      
      iface vlan_927 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_927 type vlan id 927
        up ip link set dev vlan_927 up
      
        down ip link set dev vlan_927 down
        down ip link del dev vlan_927
      
      iface vlan_928 inet manual
      
        up ifup bond0
        up ip link add link bond0 name vlan_928 type vlan id 928
        up ip link set dev vlan_928 up
      
        down ip link set dev vlan_928 down
        down ip link del dev vlan_928
    target: "/etc/network/interfaces.vlan"
    mode: "0644"
    owner: "root"
    group: "root"

- import_tasks: "{{busybox_tools}}/write_plain_file.yml"
  when:
  - "platform is defined and platform == '?'"
  vars:
    content: |
      #	This file contains a VLAN-interface configuration for a machine on an
      #	unknown platform.
      
      iface vlan_902 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_902 type vlan id 902
        up ip link set dev vlan_902 up
      
        down ip link set dev vlan_902 down
        down ip link del dev vlan_902
      
      iface vlan_921 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_921 type vlan id 921
        up ip link set dev vlan_921 up
      
        down ip link set dev vlan_921 down
        down ip link del dev vlan_921
      
      iface vlan_922 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_922 type vlan id 922
        up ip link set dev vlan_922 up
      
        down ip link set dev vlan_922 down
        down ip link del dev vlan_922
      
      iface vlan_923 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_923 type vlan id 923
        up ip link set dev vlan_923 up
      
        down ip link set dev vlan_923 down
        down ip link del dev vlan_923
      
      iface vlan_927 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_927 type vlan id 927
        up ip link set dev vlan_927 up
      
        down ip link set dev vlan_927 down
        down ip link del dev vlan_927
      
      iface vlan_928 inet manual
      
        up ifup eth0
        up ip link add link eth0 name vlan_928 type vlan id 928
        up ip link set dev vlan_928 up
      
        down ip link set dev vlan_928 down
        down ip link del dev vlan_928
    target: "/etc/network/interfaces.vlan"
    mode: "0644"
    owner: "root"
    group: "root"

#### DOC BLOCK BEGIN

# ### Bridge interfaces
#

# Try to ensure that for each logical Ethernet segment in our cloud
# environment, a bridge interface on the appropriate VLAN interface exists and
# is up.
#

#### DOC BLOCK END

- import_tasks: "{{busybox_tools}}/write_plain_file.yml"
  vars:
    content: |
      #	This file contains a bridge-interface configuration.
      
      auto virtual_hosts
      iface virtual_hosts inet manual
      
        up ifup vlan_902
        up brctl addbr virtual_hosts
        up brctl addif virtual_hosts vlan_902
        up ip link set dev virtual_hosts up
      
        down ip link set dev virtual_hosts down
        down brctl delif virtual_hosts vlan_902
        down brctl delbr virtual_hosts
        down ifdown vlan_902
      
      auto test
      iface test inet manual
      
        up ifup vlan_921
        up brctl addbr test
        up brctl addif test vlan_921
        up ip link set dev test up
      
        down ip link set dev test down
        down brctl delif test vlan_921
        down brctl delbr test
        down ifdown vlan_921
      
      auto services
      iface services inet manual
      
        up ifup vlan_922
        up brctl addbr services
        up brctl addif services vlan_922
        up ip link set dev services up
      
        down ip link set dev services down
        down brctl delif services vlan_922
        down brctl delbr services
        down ifdown vlan_922
      
      auto bscale_dev_vm
      iface bscale_dev_vm inet manual
      
        up ifup vlan_923
        up brctl addbr bscale_dev_vm
        up brctl addif bscale_dev_vm vlan_923
        up ip link set dev bscale_dev_vm up
      
        down ip link set dev bscale_dev_vm down
        down brctl delif bscale_dev_vm vlan_923
        down brctl delbr bscale_dev_vm
        down ifdown vlan_923
      
      auto ops_cluster
      iface ops_cluster inet manual
      
        up ifup vlan_927
        up brctl addbr ops_cluster
        up brctl addif ops_cluster vlan_927
        up ip link set dev ops_cluster up
      
        down ip link set dev ops_cluster down
        down brctl delif ops_cluster vlan_927
        down brctl delbr ops_cluster
        down ifdown vlan_927
      
      auto storage_net
      iface storage_net inet manual
      
        up ifup vlan_928
        up brctl addbr storage_net
        up brctl addif storage_net vlan_928
        up ip link set dev storage_net up
      
        down ip link set dev storage_net down
        down brctl delif storage_net vlan_928
        down brctl delbr storage_net
        down ifdown vlan_928
    target: "/etc/network/interfaces.bridge"
    mode: "0644"
    owner: "root"
    group: "root"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "virtual_hosts"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "test"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "services"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "bscale_dev_vm"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "ops_cluster"

- import_tasks: "{{busybox_tools}}/run_state_maintenance_command.yml"
  vars:
    command: >
      "{{commands_target_dir}}/ensure_interface_is_up"
      "storage_net"
...
